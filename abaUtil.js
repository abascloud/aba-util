'use strict';

(function () {


	/**
	* @memberOf ABAS
	*/
	class Util {

		constructor() {
			if (!Util.instance) {
				Util.instance = this;
			}
			return Util.instance;
		}

		/**
		 * Test if arguments contain undefined
		 * @param the arguments object to test
		 * return Boolean
		 */
		allDefined() {
			if (typeof arguments === 'undefined') { return false; }
			let foundUndefined = [...arguments[0]].some(a => {
				return a === null || (typeof a === 'undefined');
			});
			return !foundUndefined;
		}

		/**
		 * STOP USING THIS please use official UUID npm module
		 * @returns {string}
		 */
		generateUuid() {
			return 'xxxxxxxx-xxxx-xxxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
				var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
				return v.toString(16);
			})
		}

		/**
		 * Makes a DeepCopy
		 * @param {object} node
		 * @return {object|array}
		 */
		copy(node) {
			return JSON.parse(JSON.stringify(node));
		}

		/**
		 * Makes a KebabCase String to CamelCase
		 * @param {string} str
		 * @return {string}
		 */
		toCamelCase(str) {
			return str.replace(/-([a-z])/g, (m, w) => {
				return w.toUpperCase();
			});
		}

		isTouchDevice() {
			return (('ontouchstart' in window)
				|| (navigator.maxTouchPoints > 0)
				|| (navigator.msMaxTouchPoints > 0));
		}

		isNotTouchDevice() {
			return !this.isTouchDevice();
		}

		/**
		 * Legacy do not use
		 */
		generateWidgetId() {
			return 'widget_' + this.generateUuid();
		}

		/**
		 * Legacy do not use
		 */
		generateContainerId() {
			return 'container_' + this.generateUuid();
		}

		/**
		 * Legacy do not use
		 */
		generateDashboardId() {
			return 'dashboard_' + this.generateUuid();
		}

		getDevice() {
			var screen = window.screen;
			if (this.isNotTouchDevice()) {
				return 'desktop'
			} else {
				if (this.getOrientation() === 'portrait') {
					if (screen.availWidth < 960 && screen.availWidth > 480) {
						return 'tablet';
					} else {
						return 'smartphone';
					}
				} else {
					if (screen.availWidth < 1280 && screen.availWidth > 960) {
						return 'tablet';
					} else {
						return 'smartphone';
					}
				}
			}
		}

		getDeviceDimension() {
			var screen = window.screen;
			return this.getDimension(screen.availWidth);
		}

		getDimension(width) {
			if (width >= 1600) {
				return 'xl';
			} else if (width >= 1280 && width < 1600) {
				return 'l';
			} else if (width >= 960 && width < 1280) {
				return 'm';
			} else if (width >= 480 && width < 960) {
				return 's';
			} else {
				return 'xs';
			}/*else if(width >= 200 && width < 480) {
				   return "xs"
			   } else {
				   return "icon"
			   }*/
		}

		getOrientation() {
			var screen = window.screen;
			if (screen.availWidth < screen.availHeight) {
				return 'portrait';
			} else {
				return 'landscape';
			}
		}

		/**
		 * This simply won't work anymore.
		 * There is no alternative
		 * @deprecated
		 */
		getBaseUrl() {
			var origin = window.location.origin;
			var pathname = window.location.pathname.split('/');
			pathname = pathname.splice(0, 3).join('/');
			return origin + pathname;
		}
	}

	// export module
	if (!window.ABAS) {
		window.ABAS = {};
	}
	const instance = new Util();
	Object.freeze(instance);
	window.ABAS.util = instance;
}());