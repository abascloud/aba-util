# ABAS.util
## Description
This class provides a collection of often used utility functions

## Methods
* allDefined(arguments) - Test if all arguments are defined
* copy(Any) - makes a deep copy *maybe a bit slow
* toCamelCase(Sting) - makes a String camelcased
* isTouchDevice() - Returns true when current device has a touch screen
* isNotTouchDevice() - Returns true when current device has no touch screen
* getDevice() - Returns either [desktop, tablet, smartphone]
* getDeviceDimension() - Returns either [xl, l, m, s, xs] for the current device
* getDimension(width) -  Returns either [xl, l, m, s, xs] for given width
* getOrientation() - Returns the orientation of the current device can either be landscape or portrait
* getBaseUrl() - Return the current base url